package com.sicacho.tradingbot.reader;

import com.binance.api.client.BinanceApiClientFactory;
import com.binance.api.client.BinanceApiWebSocketClient;
import com.binance.api.client.domain.market.CandlestickInterval;
import org.ta4j.core.Bar;
import org.ta4j.core.BaseBar;
import org.ta4j.core.TimeSeries;

import java.time.Duration;
import java.time.ZonedDateTime;

/**
 * Created by truongnhukhang on 3/21/18.
 */
public class BinanceReader implements ExchangeReader {
    BinanceApiWebSocketClient client = BinanceApiClientFactory.newInstance().newWebSocketClient();
    TimeSeries timeSeries;



    @Override
    public void readBarFromExchange(String code, int candleTime) {
        client.onCandlestickEvent("btcusdt", CandlestickInterval.ONE_MINUTE, response -> {
            Bar bar = new BaseBar(Duration.ofMinutes(1), ZonedDateTime.now());
        });
    }
}
