package com.sicacho.tradingbot.reader;

import org.ta4j.core.Bar;

/**
 * Created by truongnhukhang on 3/21/18.
 */
public interface ExchangeReader {
    void readBarFromExchange(String code,int candleTime);
}
